var slider = document.getElementById('crop-bar');

noUiSlider.create(slider, {
	start: [10, 40],
	connect: true,
  behaviour: 'drag',
  margin: 1,
	range: {
		'min': 0,
		'max': 100
	}
});
